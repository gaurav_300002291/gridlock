#!/usr/bin/env python
import web
import json
import MySQLdb
from random import randint

urls = (
    '/data', 'getData',
)

app = web.application(urls, globals())

class Database:

    host = 'localhost'
    user = 'root'
    password = 'root'
    db = 'hackathon'

    def __init__(self):
        self.connection = MySQLdb.connect(self.host, self.user, self.password, self.db)
        self.cursor = self.connection.cursor()

    def insert(self, query):
        try:
            self.cursor.execute(query)
            self.connection.commit()
        except:
            self.connection.rollback()


    def query(self, query):
        cursor = self.connection.cursor( MySQLdb.cursors.DictCursor )
        cursor.execute(query)

        return cursor.fetchall()

    def __del__(self):
        self.connection.close()

class getData:
    def GET(self):
       web.header('Access-Control-Allow-Origin',      '*')
       web.header('Access-Control-Allow-Credentials', 'true')
       web.header('Content-Type', 'application/json')
       db = Database()
      
       q = web.input(group=-1)
       groupId = q['group']

       routes = ""
       if groupId == -1:
          for i in range(0, 100):
                rand = str(randint(0, 300))
                if i == 0 :
                     routes = routes + str(rand)
                else: 
                     routes = routes + "," + str(randint(0, 50))
          	#routeArray.append(randint(0, 50))
       else:
          print groupId 
          query = "SELECT routes from groups where id = " + groupId
          results = db.query(query)
          for result in results:
              routes = result['routes']

       query = "SELECT path, actual_time, start_time_start, start_time_end from routes where id in ( " + routes + " ) order by actual_time ASC"
       print query 
       results = db.query(query)

       finalData = {}
       list = [] 
       for result in results:
	 route = str(result['path'])
         actual_time = result['actual_time']
         start_time_start = result['start_time_start']
         start_time_end = result['start_time_end']
         query = "SELECT startLat, startLng, endLat, endLng, duration from streets where id in ( " + str(route) + " ) order by Field ( id, " + str(route) + " )"
         #print query
         results = db.query(query)
         coords = []
         durations = []
         for result in results:
            coord = {}  
            coord['lat'] = result['startLat']
            coord['lng'] = result['startLng']
            coords.append(coord)
            durations.append(result['duration'])
      	 data = {}
         data['coords'] = coords
         data['durations'] = durations
         data['actualTime'] = actual_time
         data['startTime'] = start_time_start
         data['endTime'] = start_time_end
         list.append(data)

         finalData['list'] = list
       
       json_data = json.dumps(finalData)
       return json_data


if __name__ == "__main__":
    app.run()
