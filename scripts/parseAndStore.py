import json
from pprint import pprint

import MySQLdb


class Database:

    host = 'localhost'
    user = 'root'
    password = 'root'
    db = 'hackathon'

    def __init__(self):
        self.connection = MySQLdb.connect(self.host, self.user, self.password, self.db)
        self.cursor = self.connection.cursor()

    def insert(self, query):
        try:
            self.cursor.execute(query)
            self.connection.commit()
            return self.cursor.lastrowid
        except:
            self.connection.rollback()


    def query(self, query):
        cursor = self.connection.cursor( MySQLdb.cursors.DictCursor )
        cursor.execute(query)

        return cursor.fetchall()

    def __del__(self):
        self.connection.close()


def storeData(data):
 route = data['routes'][0]
 leg = route['legs'][0]

 steps = leg['steps']
 
 routeS = ""
 globalFirst = 1
 for step in steps: 
    first = 1
    duration = step['duration']['value']
    paths = step['path']
    length = len(paths)-1
    pathDuration = duration/length
    for path in paths:
         if first == 1:
            startLat = round(path['lat'],5)
            startLng = round(path['lng'],5)
            first = 0 
         else:
            endLat = round(path['lat'],5)
            endLng = round(path['lng'],5)
            #print(str(startLat) + " " + str(startLng) +" " + str(endLat) + " " + str(endLng) +" "+ str(pathDuration) + "\n")
            searchquery = "select id from streets where startLat = " + str(startLat) + " and startLng = " + str(startLng) + " and endLat = " +  str(endLat) + " and endLng = " + str(endLng)
            ids = db.query(searchquery)
            alreadyExist = 0 
            for id in ids:
		alreadyExist = 1
	        alreadyId = id['id']
		print("Street Already Exists: " + str(id['id']))
                if globalFirst == 1:
                	routeS = str(alreadyId)
                        globalFirst = 0
                else: 
                        routeS = routeS + ", " + str(alreadyId) 

	    if alreadyExist == 0: 
            	query = "INSERT INTO streets (`startLat`, `startLng`, `endLat`, `endLng`, `duration`) VALUES ( " + str(startLat) + " , " + str(startLng) + " , " + str(endLat) + " , " + str(endLng) + " , " + str(pathDuration)  + " )"
            	id = db.insert(query)
                if globalFirst == 1:
                        routeS = str(id)
                        globalFirst = 0
                else:
                        routeS = routeS + ", " + str(id)

            startLat = endLat
            startLng = endLng
 query = "INSERT INTO routes (`path`) VALUES ( '"+ routeS + "' )"
 id = db.insert(query)

db = Database()
with open('marketshare.json') as data_file:    
    for line in data_file:
        print line
    	data = json.loads(line)
        storeData(data)

