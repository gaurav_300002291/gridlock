package com.concretepage.controller;
import com.concretepage.entity.Route;
import com.concretepage.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("user")
public class RouteController {

	@Autowired
	private RouteService routeService;

	@GetMapping("route/{id}")
	public ResponseEntity<Route> startAlgorithm(@PathVariable(value="id") Long id) {
		routeService.startAlgorithm(id);
		return new ResponseEntity<Route>(HttpStatus.OK);
	}

	@GetMapping("route")
	public ResponseEntity<Route> startAlgorithm() {
		routeService.startAlgorithm(-1L);
		return new ResponseEntity<Route>(HttpStatus.OK);
	}
} 