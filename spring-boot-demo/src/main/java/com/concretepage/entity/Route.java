package com.concretepage.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="routes")
public class Route implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private Long id;

	@Column(name="path")
    private String path;

	@Column(name = "start_time_start", nullable = true)
	private Long startTimeStart;

	@Column(name = "start_time_end", nullable = true)
	private Long startTimeEnd;

	@Column(name = "actual_time", nullable = true)
	private Long actualTime;

	@Transient
	public String[] getStreetList() {
		return path.split(",");
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getStartTimeStart() {
		return startTimeStart;
	}

	public void setStartTimeStart(Long startTimeStart) {
		this.startTimeStart = startTimeStart;
	}

	public Long getStartTimeEnd() {
		return startTimeEnd;
	}

	public void setStartTimeEnd(Long startTimeEnd) {
		this.startTimeEnd = startTimeEnd;
	}

	public Long getActualTime() {
		return actualTime;
	}

	public void setActualTime(Long actualTime) {
		this.actualTime = actualTime;
	}
}