package com.concretepage.service.impl;

import com.concretepage.dao.GroupDAO;
import com.concretepage.dao.RouteDAO;
import com.concretepage.dao.StreetDAO;
import com.concretepage.domain.Constants;
import com.concretepage.domain.Schedule;
import com.concretepage.entity.Group;
import com.concretepage.entity.Route;
import com.concretepage.entity.Street;
import com.concretepage.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RouteServiceImpl implements RouteService {

	@Autowired
	private RouteDAO routeDAO;

	@Autowired
	private StreetDAO streetDAO;

	@Autowired
	private GroupDAO groupDAO;

	@Override
	public Route getRouteById(int routeId) {
		Route route = routeDAO.getRouteById(1L);
		return route;
	}

	@Override
	public void startAlgorithm(Long groupId){

		System.out.println("Algorithm Started::");

		Random rand = new Random(System.currentTimeMillis());
		Map<Long, Route> idToRouteMap = new HashMap<>();

		if (groupId == -1) {
			for (int i = 0; i < Constants.NoOfRoutesInSchedule; i++) {
				Long routeId = new Long(Constants.RouteIdStart + rand.nextInt(Constants.RouteIdEnd - Constants.RouteIdStart));
				Route route = routeDAO.getRouteById(routeId);
				idToRouteMap.put(routeId, route);
			}
		}else{
			 Group group = groupDAO.getGroupById(groupId);
			 String[] routeIdList = group.getRouteList();
			 for(String id: routeIdList){
				 Long routeId = Long.parseLong(id);
				 Route route = routeDAO.getRouteById(routeId);
				 idToRouteMap.put(routeId, route);
			 }
		}

		System.out.println(	"Size of idToRouteMap " + idToRouteMap.size());

		List<Schedule> scheduleList = getInitialPopulation(idToRouteMap);

		for(int i = 0 ;i<Constants.NoOfGeneration; i++){
			scheduleList = crossover(scheduleList);
			scheduleList = mutation(scheduleList);
		}
		Collections.sort(scheduleList);

		Schedule finalSchedule = scheduleList.get(0);
		updateRoute(finalSchedule);

		System.out.println("The final best Fitness Score is : " + finalSchedule.getFitnessScore());
	}

	private void updateRoute(Schedule finalSchedule){
		Map<Long, Long> slotMap = finalSchedule.getSlotMap();
		for(Map.Entry<Long, Long> entry: slotMap.entrySet()){
			Long key = entry.getKey();
			Long value = entry.getValue();

			routeDAO.updateRoute(key, value);
		}
	}
	private List<Schedule> crossover(List<Schedule> scheduleList){

		int size = scheduleList.size();
		Random rand = new Random(System.currentTimeMillis());
		Collections.sort(scheduleList);

		List<Schedule> newScheduleList = new ArrayList<>();
		for(int i=0; i< Constants.NoOfCrossOverPairs*Constants.populationSize; i++) {
			int first = rand.nextInt(size);
			int second = rand.nextInt(size);
			if (first != second) {
				Schedule firstSchedule = scheduleList.get(first);
				Schedule secondSchedule = scheduleList.get(second);

				System.out.println("Crossing Over " + firstSchedule.getFitnessScore() + "with " + secondSchedule.getFitnessScore());

				Schedule newSchedule = new Schedule();
				newSchedule.setIdToRouteMap(firstSchedule.getIdToRouteMap());

				Map<Long, Long> slotMap = new HashMap<>();
				Iterator<Map.Entry<Long, Long>> iter1 = firstSchedule.getSlotMap().entrySet().iterator();
				Iterator<Map.Entry<Long, Long>> iter2 = secondSchedule.getSlotMap().entrySet().iterator();
				while (iter1.hasNext() && iter2.hasNext()) {
					Map.Entry<Long, Long> e1 = iter1.next();
					Map.Entry<Long, Long> e2 = iter2.next();
					if (rand.nextBoolean()) {
						slotMap.put(e1.getKey(), e1.getValue());
					} else {
						slotMap.put(e2.getKey(), e2.getValue());
					}
				}

				newSchedule.setSlotMap(slotMap);
				Integer fitnessScore = generateFitnessScore(newSchedule);
				newSchedule.setFitnessScore(fitnessScore);
				newScheduleList.add(newSchedule);

				System.out.println("[CrossOver] Fitness Score For the Schedule is:" + fitnessScore);

			}
		}

		int sizeOfNewPop = newScheduleList.size();
		for(int i=0; i<sizeOfNewPop; i++){
			scheduleList.set(size-1, newScheduleList.get(i));
			size--;
		}

		return scheduleList;

	}

	private List<Schedule> mutation(List<Schedule> scheduleList){

		int size = scheduleList.size();
		Random rand = new Random(System.currentTimeMillis());
		Collections.sort(scheduleList);

		int minSiz = size/2;

		for(int i=0; i<Constants.NoOfMutation*Constants.populationSize;i++) {
			int index = minSiz + rand.nextInt(size - minSiz);

			Schedule newSchedule = scheduleList.get(index);
			System.out.println("Mutating schedule " + newSchedule.getFitnessScore());

			Map<Long, Long> slotMap = newSchedule.getSlotMap();

			int routeSiz = slotMap.size();

			int routeIndex = rand.nextInt(routeSiz);
			int j = 0;

			for (Map.Entry<Long, Long> entry : slotMap.entrySet()) {
				Long key = entry.getKey();
				Long value = entry.getValue();
				if (j == routeIndex) {
					Route route = newSchedule.getIdToRouteMap().get(key);
					Long endTime = route.getStartTimeEnd();
					Long diffTime = endTime-value;
					int diff = diffTime.intValue();
					Long randomSecs = 0L;
					if(diff > 0) {
						int randNumber = rand.nextInt(diff);
						randomSecs = new Long(randNumber);
//					System.out.println("Inside Rand Generator::" + randNumber + "random Secs::" + randomSecs);
					}
					slotMap.put(key, value + randomSecs);
					newSchedule.setSlotMap(slotMap);
					Integer fitnessScore = generateFitnessScore(newSchedule);
					newSchedule.setFitnessScore(fitnessScore);
					System.out.println("[Mutation] Fitness Score For the Schedule is:" + fitnessScore);
				}
				j = j + 1;
			}
			scheduleList.set(index, newSchedule);

		}
		return scheduleList;
	}

	private List<Schedule> getInitialPopulation(Map<Long, Route> idToRouteMap){

		List<Schedule> scheduleList = new ArrayList<>();
		Random rand = new Random(System.currentTimeMillis());

		for(int i=0; i<Constants.populationSize; i++ ) {
			Schedule schedule = new Schedule();
			Map<Long, Long> slotMap = new HashMap<>();

			for (Map.Entry<Long, Route> entry : idToRouteMap.entrySet()) {
				Route route = entry.getValue();
				Long startTime = route.getStartTimeStart();
				Long endTime = route.getStartTimeEnd();
				Long diffTime = endTime - startTime;
				int diff = diffTime.intValue();
				Long randomSecs = 0L;
				if(diff > 0) {
					int randNumber = rand.nextInt(diff);
					randomSecs = new Long(randNumber);
//					System.out.println("Inside Rand Generator::" + randNumber + "random Secs::" + randomSecs);
				}

//				long min = (startTime+randomSecs)/60;
//				long hr = min/60;
//				long remMin = min%60;

				slotMap.put(entry.getKey(), startTime + randomSecs);
//				System.out.println("Start time for Route: " + route.getId() + "is: " + hr + ":" + remMin );
			}
			schedule.setSlotMap(slotMap);
			schedule.setIdToRouteMap(idToRouteMap);

			Integer fitnessScore = generateFitnessScore(schedule);
			schedule.setFitnessScore(fitnessScore);

			scheduleList.add(schedule);
			System.out.println("While Generating the Schedule. Fitness Score For the Schedule is:" + fitnessScore);
		}

		return scheduleList;

	}

	private Integer generateFitnessScore(Schedule schedule){

		Map<Long, Long> slotMap = schedule.getSlotMap();
		Map<Long, List<Integer>> streetTimeMap = new HashMap<>();
		Map<Long, Route> idToRouteMap = schedule.getIdToRouteMap();

		for(Map.Entry<Long, Long> entry : slotMap.entrySet()) {
			//Route route = routeDAO.getRouteById(entry.getKey());
			Route route = idToRouteMap.get(entry.getKey());
			List<Street> streetList = streetDAO.getStreetsFromRoute(route.getPath());

			Long startTime = entry.getValue();
			for(Street street:streetList){
				startTime = startTime + street.getDuration();
				Long reachTime = startTime/300;
				Integer slot =  reachTime.intValue();
				if(streetTimeMap.containsKey(street.getId())){
					List<Integer> countOfEachTime = streetTimeMap.get(street.getId());
					countOfEachTime.set(slot, countOfEachTime.get(slot) + 1);
				}else {
					List<Integer> countOfEachTime = new ArrayList<>(Collections.nCopies(289, 0));
					countOfEachTime.set(slot,1);
					streetTimeMap.put(street.getId(), countOfEachTime);
				}
			}

			//System.out.println("Total Travel Duration for Route: " + route.getId() + "is : " + startTime);

		}

		Integer fitnessScore = 0;
		for(Map.Entry<Long, List<Integer>> entry: streetTimeMap.entrySet()){
			List<Integer> timeSlotList = entry.getValue();
			Integer index = 0;
			for(Integer slotCount: timeSlotList){
				if(slotCount > 1) {
					//System.out.println("Collision For Street: " + entry.getKey() + "At time Slot:" + index);
					fitnessScore = fitnessScore + (slotCount - 1);
				}
				index++;
			}
		}

		return fitnessScore;

	}
}
