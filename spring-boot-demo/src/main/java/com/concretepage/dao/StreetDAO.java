package com.concretepage.dao;

import com.concretepage.entity.Street;

import java.util.List;

public interface StreetDAO {

    public Street getArticleById(int articleId);

    public List<Street> getStreetsFromRoute(String route);

}
 