package com.concretepage.dao;

import com.concretepage.entity.Route;
import com.concretepage.entity.Street;

public interface RouteDAO {

    public Route getRouteById(Long routeId);

    public void updateRoute(Long id, Long actualTime);

}
 