package com.concretepage.dao;

import com.concretepage.entity.Group;
import com.concretepage.entity.Street;

import java.util.List;

public interface GroupDAO {

    public Group getGroupById(Long groupId);

}
 