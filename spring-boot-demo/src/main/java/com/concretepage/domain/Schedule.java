package com.concretepage.domain;

import com.concretepage.entity.Route;

import java.util.Map;

public class Schedule implements Comparable<Schedule> {

    private Map<Long, Long> slotMap;

    private Map<Long, Route> idToRouteMap;

    private Integer fitnessScore;

    public Map<Long, Long> getSlotMap() {
        return slotMap;
    }

    public void setSlotMap(Map<Long, Long> slotMap) {
        this.slotMap = slotMap;
    }

    public Integer getFitnessScore() {
        return fitnessScore;
    }

    public void setFitnessScore(Integer fitnessScore) {
        this.fitnessScore = fitnessScore;
    }

    public Map<Long, Route> getIdToRouteMap() {
        return idToRouteMap;
    }

    public void setIdToRouteMap(Map<Long, Route> idToRouteMap) {
        this.idToRouteMap = idToRouteMap;
    }

    public int compareTo(Schedule o) {
        return fitnessScore<o.getFitnessScore()?-1:fitnessScore>o.getFitnessScore()?1:0;
    }
}
