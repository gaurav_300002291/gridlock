package com.concretepage.domain;

public class Constants {

    public static int populationSize  = 100;

    public static int NoOfGeneration = 100;

    public static Double NoOfCrossOverPairs = 0.2;  // should be much less than half the population Size

    public static Double NoOfMutation = 0.1;  // should be very less than population Size

    public static int NoOfRoutesInSchedule = 50;

    public static int RouteIdStart = 1;

    public static int RouteIdEnd = 50;


}
